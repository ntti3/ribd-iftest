package main

import (
	"context"
	"fmt"
	"github.com/coreswitch/log"
	"github.com/vishvananda/netlink"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"time"
)

const (
	defRepeat = 1
	scriptDir = "/persist/scripts"
	startPath = scriptDir + "/" + "start.sh"
	stopPath  = scriptDir + "/" + "stop.sh"
	timeout   = 10 * time.Second
)

const startScript = `#!/bin/sh

echo ribd 1>&2        && systemctl start ribd
echo gobgpd 1>&2      && systemctl start gobgpd
echo openconfigd 1>&2 && systemctl start openconfigd
`
const stopScript = `#!/bin/sh

echo openconfigd 1>&2 && systemctl stop openconfigd
echo gobgpd 1>&2      && systemctl stop gobgpd
echo ribd 1>&2        && systemctl stop ribd
`

type script struct {
	path string
	name string
}

type intf struct {
	name  string
	found bool
}

type vrf struct {
	name string
	ifs  []*intf
}

func makeScripts() {
	var scripts []*script

	scripts = append(scripts, &script{
		path: startPath,
		name: startScript})
	scripts = append(scripts, &script{
		path: stopPath,
		name: stopScript})

	if err := os.MkdirAll(scriptDir, 0755); err != nil {
		if !os.IsExist(err) {
			log.Errorf("MkdirAll(%s): %v", err)
			os.Exit(1)
		}
	}
	for _, script := range scripts {
		if err := ioutil.WriteFile(script.path, []byte(script.name), 0755); err != nil {
			log.Errorf("WriteFile(%s): %v", script.path, err)
			os.Exit(1)
		}
	}
}

func getVrfs() ([]*netlink.Vrf, error) {
	var vrfs []*netlink.Vrf

	if ll, err := netlink.LinkList(); err == nil {
		for _, l := range ll {
			switch l.(type) {
			case *netlink.Vrf:
				vrfs = append(vrfs, l.(*netlink.Vrf))
			}
		}
		return vrfs, nil
	} else {
		return vrfs, err
	}
}

func getVrfByName(name string) (*netlink.Vrf, error) {
	if l, err := netlink.LinkByName(name); err == nil {
		switch l.(type) {
		case *netlink.Vrf:
			return l.(*netlink.Vrf), nil
		}
		return nil, fmt.Errorf("getVrfByName(%s): not a VRF", name)
	} else {
		log.Errorf("getVrfByName(%s): %v", name, err)
		return nil, err
	}
}

func getVrfFromVlan(vlan *netlink.Vlan) (*netlink.Vrf, error) {
	mi := vlan.Attrs().MasterIndex
	name := vlan.Attrs().Name
	if mi > 0 {
		if l, err := netlink.LinkByIndex(mi); err == nil {
			switch l.(type) {
			case *netlink.Vrf:
				return l.(*netlink.Vrf), nil
			}
			return nil, fmt.Errorf(
				"Error: getVrfFromVlan(): %s is not VRF.", l.Attrs().Name)
		} else {
			log.Errorf("Error: getVrfFromVlan(%s): LinkByIndex(%d): %v",
				name, mi, err)
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Error: getVrfFromVlan(%s): %d", name, mi)
	}
}

func getVrfFromIntfByName(name string) (*netlink.Vrf, error) {
	if l, err := netlink.LinkByName(name); err == nil {
		switch l.(type) {
		case *netlink.Vlan:
			return getVrfFromVlan(l.(*netlink.Vlan))
		}
		return nil,
			fmt.Errorf("Error: getvrffromintfbyname(%s): not vlan", name)
	} else {
		return nil, fmt.Errorf("Getvrffromintfbyname(%s): %v", name, err)
	}
}

func getVlans(vrf *vrf) error {
	if links, err := netlink.LinkList(); err == nil {
		for _, link := range links {
			switch link.(type) {
			case *netlink.Vlan:
				mi := link.Attrs().MasterIndex
				if mi > 0 {
					if l, err := netlink.LinkByIndex(mi); err == nil {
						switch l.(type) {
						case *netlink.Vrf:
							if l.Attrs().Name == vrf.name {
								vrf.ifs = append(vrf.ifs, &intf{
									name:  link.Attrs().Name,
									found: false,
								})
							}
						}
					} else {
						log.Errorf("getVlans(): Error: %v", err)
					}
				}
			}
		}
		return nil
	} else {
		return err
	}
}

func checkVrf(vrf *vrf) bool {
	if ll, err := netlink.LinkList(); err == nil {
		for _, l := range ll {
			switch l.(type) {
			case *netlink.Vrf:
				if l.Attrs().Name == vrf.name {
					return true
				}
			}
		}
		return false
	} else {
		log.Errorf("checkVrf(%s): %v", vrf.name, err)
		return false
	}
}

func checkVrfs(vrfs []*vrf, mustExist bool) bool {
	rc := true
	for _, vrf := range vrfs {
		fmt.Fprintf(os.Stderr, "        verifying %s... ", vrf.name)
		if checkVrf(vrf) {
			if mustExist {
				fmt.Fprintf(os.Stderr, "exists (correct).\n")
			} else {
				rc = false
				fmt.Fprintf(os.Stderr, "exists (ERROR).\n")
			}
		} else {
			if mustExist {
				rc = false
				fmt.Fprintf(os.Stderr, "does not exist (ERROR).\n")
				//os.Exit(1)
			} else {
				fmt.Fprintf(os.Stderr, "does not exist (correct).\n")
			}
		}
	}
	return rc
}

func checkVlanVrf(l netlink.Link, vrf *vrf) (bool, error) {
	var (
		err    error
		errMsg = fmt.Sprintf(
			"Error: checkVlanVrf(%s, %s): ", l.Attrs().Name, vrf.name)
		origVrf *netlink.Vrf
		vlanVrf *netlink.Vrf
	)
	if origVrf, err = getVrfByName(vrf.name); err != nil {
		log.Errorf(errMsg+"getVrfByName(%s): %v", vrf.name, err)
		return false, err
	}
	switch l.(type) {
	case *netlink.Vlan:
		if vlanVrf, err = getVrfFromVlan(l.(*netlink.Vlan)); err != nil {
			log.Errorf(errMsg+"getVrfFromVlan(): %v", err)
			return false, err
		}
	}
	for _, intf := range vrf.ifs {
		if l.Attrs().Name == intf.name {
			if vlanVrf.Table == origVrf.Table {
				intf.found = true
				return true, nil
			} else {
				log.Errorf(errMsg+"l: %v, vrf: %v", l, vlanVrf)
			}
		}
	}
	//log.Infof("checkVlanVrf(): %s not in %s", l.Attrs().Name, vrf.name)
	return false, nil
}

func checkVlans(vrf *vrf, mustExist bool) bool {
	var (
		rc           = true
		existIsError = !mustExist
	)

	for _, intf := range vrf.ifs {
		intf.found = false
	}
	fmt.Fprintf(os.Stderr, "        collecting VLANs... ")
	if links, err := netlink.LinkList(); err == nil {
		for _, link := range links {
			for _, intf := range vrf.ifs {
				if link.Attrs().Name == intf.name {
					if existIsError {
						log.Errorf(
							"checkVlans(%s): must not exist.\n", intf.name)
						rc = false
						//os.Exit(1)
					}
					intf.found = true
				}
			}
		}
		for _, intf := range vrf.ifs {
			if intf.found {
				if existIsError {
					log.Errorf("checkVlans(%s): must not exist.\n", intf.name)
					rc = false
					//os.Exit(1)
				} else {
					fmt.Fprintf(os.Stderr, "%s ", intf.name)
				}
			} else {
				if mustExist {
					log.Errorf("checkVlans(%s): must exist.\n", intf.name)
					rc = false
					//os.Exit(1)
				}
			}
		}
		fmt.Fprintf(os.Stderr, "DONE.\n")
		return rc
	} else {
		log.Errorf("checkVlans(%s): LInkList(): %v", vrf.name, err)
		return false
	}
}

func checkVlansVrf(vrf *vrf, mustExist bool) bool {
	var (
		existIsError = !mustExist
	)

	for _, intf := range vrf.ifs {
		intf.found = false
	}
	fmt.Fprintf(os.Stderr, "        collecting VLANs in VRF %s... ", vrf.name)
	if links, err := netlink.LinkList(); err == nil {
		for _, link := range links {
			if ok, err := checkVlanVrf(link, vrf); err == nil {
				if ok {
					fmt.Fprintf(os.Stderr, "%s ", link.Attrs().Name)
					if existIsError {
						log.Errorf("checkVlansVrf(%s): %s must not exist.\n",
							vrf.name, link.Attrs().Name)
						os.Exit(1)
					}
				}
			} else {
				log.Errorf(
					"checkVlansVrf(%s): checkVlanVrf(): %v\n", vrf.name, err)
			}
		}
		if existIsError {
			log.Errorf("checkVlansVrf(%s, %v): existIsError must be false.\n",
				vrf.name, existIsError)
			os.Exit(1)
		}
		for _, intf := range vrf.ifs {
			if intf.found == false {
				log.Errorf("checkVlansVrf(%s): must exist.\n", intf.name)
				os.Exit(1)
			}
		}
		fmt.Fprintf(os.Stderr, "DONE.\n")
		return true
	} else {
		log.Errorf("checkVlansVrf(%s): %v: %v\n", vrf.name, vrf.ifs, err)
		return false
	}
}

func ctlRibd(script string) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	return exec.CommandContext(ctx, script).Run()
}

func main() {
	log.FuncField = false
	log.SourceField = false
	log.SetTextFormatter()
	log.SetLevel("debug")

	var (
		n    int
		ok   bool
		vrfs []*vrf
	)

	if len(os.Args) > 1 {
		if i, err := strconv.Atoi(os.Args[1]); err == nil {
			n = i
		} else {
			fmt.Fprintf(os.Stderr,
				"Error: Strconv.Atoi(%s): %v", os.Args[1], err)
			os.Exit(1)
		}
	} else {
		n = defRepeat
	}
	fmt.Printf("Repeating %d times\n", n)

	makeScripts()

	if vv, err := getVrfs(); err == nil {
		for _, v := range vv {
			vrfs = append(vrfs, &vrf{name: v.Attrs().Name})
		}
		for _, v := range vrfs {
			log.Debugf("vrf: %s", v.name)
			getVlans(v)
			for _, intf := range v.ifs {
				log.Debug(intf)
			}
		}
	}

	for i := 0; i < n; i++ {
		fmt.Fprintf(os.Stderr, "%4d: stop\n", i)
		if err := ctlRibd(stopPath); err != nil {
			log.Errorf("Error: ctrRibd(%s): %v", stopPath, err)
			os.Exit(1)
		}
		ok = true
		//
		// VRFs must *NOT* exist
		//
		if rc := checkVrfs(vrfs, false); !rc {
			ok = false
		}
		for _, vrf := range vrfs {
			//
			// VLANs must *NOT* exist
			//
			if rc := checkVlans(vrf, false); !rc {
				ok = false
			}
		}
		if !ok {
			os.Exit(1)
		}
		fmt.Fprintf(os.Stderr, "%4d: start\n", i)
		if err := ctlRibd(startPath); err != nil {
			log.Errorf("Error: ctrRibd(%s): %v", startPath, err)
			os.Exit(1)
		}
		time.Sleep(10 * time.Second)
		//
		// VRFs must exist
		//
		if rc := checkVrfs(vrfs, true); !rc {
			ok = false
		}
		for _, vrf := range vrfs {
			//
			// VLANs must exist
			//
			if rc := checkVlansVrf(vrf, true); !rc {
				ok = false
			}
		}
		if !ok {
			os.Exit(1)
		}
	}
}
